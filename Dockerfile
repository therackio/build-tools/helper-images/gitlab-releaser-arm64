ARG PARENT_IMAGE
FROM $PARENT_IMAGE

LABEL maintainer="gitlab@therack.io"

WORKDIR /

RUN \
    set -eux && \
    apk add --no-cache aws-cli curl && \
    release-cli --version && \
    aws --version
